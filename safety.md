It will be possible to edit a petri net while simulating it.
[simple-petri-net](https://gitlab.com/porky11/simple-petri-net) already allows this by introducing a high level langauge abstraction for petri nets, but when editing petri nets directly, there have to be new safety restrictions.

Safety in this context means, that every route, that was possible before some edit, will still be possible after that edit.

These safety restrictions are not only useful for interactive editing, but also for expanding stories after creation without having to worry to change existing story lines, for example when adding new content to a game.

There are the following restrictions:

* adding places or transitions is always safe
* removing transitions is always unsafe
* removing places is always safe
* adding connections from places to transitions is only save, when the transition is new
* removing connections from places to transitions is always safe
* adding connections from transitions to places is always safe
* removing connections from transitions to places is always unsafe
* duplicating places (adding places and connecting them with the same input and output transitions as another place) is always safe
* duplicating transitions (adding transitions and connecting them with the same input and output places as another place) is always safe
* increasing initial token counts is always safe
* decreasing initial token counts is only safe, when the places don't have any output transitions

In order to ensure the safety, tools for ensuring safety need to be implemented in the C API already.
If an action is never safe, it is not required to expose it in the API, but some trivial methods will.
If an action is always safe, it will be exposed by the API.
If an action is sometimes safe, two functions will be exposed by the API, one to check for safety and one to execute the action.

