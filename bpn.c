#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bpn.h"

/* MACROS */

#define CHECK_MEMORY_ERROR(result) if (!result) {\
    fprintf(stderr, "Out of memory");\
    exit(EXIT_FAILURE);\
}

#define RESIZE_ARRAY(count, size, value) if (count > size) {\
    size = count + count / 5;\
    value = realloc(value, size * sizeof(*value));\
    CHECK_MEMORY_ERROR(value);\
}

#define ADD_ELEMENT(name, count, size, value) {\
    name = count++;\
    RESIZE_ARRAY(count, size, value)\
}

/* UNEXPORTED */

/** Node **/

void bpnCreateNode(BpnNode* node) {
    node->prev_count = 0;
    node->prev_size = 0;
    node->prev = NULL;
    node->next_count = 0;
    node->next_size = 0;
    node->next = NULL;
}

void bpnCloneNode(BpnNode* node_clone, const BpnNode* node) {
    node_clone->prev_count = node->prev_count;
    node_clone->prev_size = node->prev_size;
    node_clone->prev = malloc(node->prev_size * sizeof(size_t));
    CHECK_MEMORY_ERROR(node_clone->prev);
    memcpy(node_clone->prev, node->prev, node->prev_count * sizeof(size_t));
    node_clone->next_count = node->next_count;
    node_clone->next_size = node->next_size;
    node_clone->next = malloc(node->next_size * sizeof(size_t));
    CHECK_MEMORY_ERROR(node_clone->next);
    memcpy(node_clone->next, node->next, node->next_count * sizeof(size_t));
}

void bpnDeleteNode(BpnNode* node) {
    free(node->prev);
    free(node->next);
    bpnCreateNode(node);
}

/** IndexList **/

void BpnIndexList_push(BpnIndexList** place, size_t index) {
    BpnIndexList* new_list = malloc(sizeof(BpnIndexList));
    CHECK_MEMORY_ERROR(new_list);
    new_list->index = index;
    new_list->next = *place;
    *place = new_list;
}

void bpnCloneIndexList(BpnIndexList** clone_place, BpnIndexList* list) {
    while (list != NULL) {
        BpnIndexList* new_list = malloc(sizeof(BpnIndexList));
        CHECK_MEMORY_ERROR(new_list);
        new_list->index = list->index;
        new_list->next = NULL;
        *clone_place = new_list;
        clone_place = &new_list->next;
        list = list->next;
    }
}

size_t BpnIndexList_pop(BpnIndexList** place) {
    size_t tid = (*place)->index;
    BpnIndexList* next = (*place)->next;
    free(*place);
    *place = next;
    return tid;
}

bool BpnIndexList_appendNew(BpnIndexList** place, size_t tid) {
start:
    if (*place == NULL) {
        BpnIndexList_push(place, tid);
        return true;
    }
    else if ((*place)->index == tid) return false;
    else place = &(*place)->next;
    goto start;
}

bool BpnIndexList_contains(BpnIndexList* list, size_t index) {
    while (list) {
        if (list->index == index) return true;
        list = list->next;
    }
    return false;
}

bool BpnIndexList_remove(BpnIndexList** place, size_t tid) {
start:
    if (*place == NULL) return false;
    else if ((*place)->index == tid) {
        BpnIndexList* current = *place;
        *place = (*place)->next;
        free(current);
        return true;
    }
    else place = &(*place)->next;
    goto start;
}

void bpnDestroyIndexList(BpnIndexList* list) {
    while (list) {
        BpnIndexList* current = list;
        list = list->next;
        free(current);
    }
}

void bpnClearIndexList(BpnIndexList** list) {
    bpnDestroyIndexList(*list);
    *list = NULL;
}

/** Fire Changes **/

void bpnCreateFireChanges(BpnFireChanges* changes) {
    changes->count = 0;
    changes->added_count = 0;
    changes->removed_count = 0;
    changes->active = NULL;
    changes->added = NULL;
    changes->removed = NULL;
}

void bpnDestroyFireChanges(BpnFireChanges* changes) {
    bpnDestroyIndexList(changes->active);
    bpnDestroyIndexList(changes->added);
    bpnDestroyIndexList(changes->removed);
}

void bpnFireChanges_add(BpnFireChanges* changes, size_t tid) {
    bool added = BpnIndexList_appendNew(&changes->active, tid);
    changes->count += added;
    if (added) {
        if (!BpnIndexList_remove(&changes->removed, tid)) {
            ++changes->added_count;
            BpnIndexList_push(&changes->added, tid);
        }
        else --changes->removed_count;
    }
}
void bpnFireChanges_remove(BpnFireChanges* changes, size_t tid) {
    bool removed = BpnIndexList_remove(
        &changes->active,
        tid
    );
    changes->count -= removed;
    if (removed) {
        if (!BpnIndexList_remove(&changes->added, tid)) {
            ++changes->removed_count;
            BpnIndexList_push(&changes->removed, tid);
        }
        else --changes->added_count;
    }
}

/** helpers **/

/*** Array ***/

size_t sortIndex(size_t id, size_t count, size_t* ids) {
    for (size_t i = 0; i < count; ++i) {
        size_t current_id = ids[i];
        if (current_id > id) {
            ids[i] = id;
            id = current_id;
        }
    }
    return id;
}

void removeIndex(size_t id, size_t* count, size_t* ids) {
    if (!*count) return;
    --*count;

    size_t i = 0;
    if (ids[*count] == id) return;
    while (i < *count) {
        if (ids[i] == id) {
            ids[i] = ids[i + 1];
            break;
        }
        ++i;
    }
    if (i == *count) {
        ++*count;
        return;
    }
    while (++i < *count) ids[i] = ids[i + 1];
}

/*** token calculations ***/

bool activated(const BpnState* state, const BpnNet* net, size_t pid) {
    return state->activated[pid] ^ net->initial_activated[pid];
}

void addFireable(BpnState* state, size_t tid) {
    bpnFireChanges_add(&state->fire, tid);
}

void removeFireable(BpnState* state, size_t tid) {
    bpnFireChanges_remove(&state->fire, tid);
}

void addUnfireable(BpnState* state, size_t tid) {
    bpnFireChanges_add(&state->unfire, tid);
}

void removeUnfireable(BpnState* state, size_t tid) {
    bpnFireChanges_remove(&state->unfire, tid);
}

typedef struct FireableResult {
    bool fireable, unfireable;
} FireableResult;

FireableResult checkFireable(BpnState* state, const BpnNet* net, size_t tid) {
    BpnNode* transition = &net->transitions[tid];
    FireableResult result = { true, true };

    if (!transition->prev_count || !transition->next_count) {
        result.fireable = false;
        result.unfireable = false;
    }
    else {
        BpnIndexList* duplicate_indices = NULL;

        for (size_t pi = 0; pi < transition->prev_count; ++pi) {
            size_t ppid = transition->prev[pi];
            for (size_t ni = 0; ni < transition->next_count; ++ni) {
                size_t npid = transition->next[ni];
                if (ppid == npid) BpnIndexList_push(&duplicate_indices, npid);
            }
        }

        for (size_t i = 0; i < transition->prev_count; ++i) {
            if (!result.fireable && !result.unfireable) break;

            size_t pid = transition->prev[i];

            if (BpnIndexList_contains(duplicate_indices, pid)) continue;

            if (activated(state, net, pid)) result.unfireable = false;
            else result.fireable = false;
        }

        for (size_t i = 0; i < transition->next_count; ++i) {
            if (!result.fireable && !result.unfireable) break;

            size_t pid = transition->next[i];

            if (BpnIndexList_contains(duplicate_indices, pid)) continue;

            if (activated(state, net, pid)) result.fireable = false;
            else result.unfireable = false;
        }

        while (duplicate_indices) {
            size_t pid = duplicate_indices->index;
            if (!activated(state, net, pid)) {
                result.fireable = false;
                result.unfireable = false;
            }
            BpnIndexList* current = duplicate_indices;
            duplicate_indices = duplicate_indices->next;
            free(current);
        }
    }

    return result;
}

void recalculate_transition(BpnState* state, const BpnNet* net, size_t tid) {
    FireableResult now = checkFireable(state, net, tid);

    if (now.fireable) addFireable(state, tid);
    else removeFireable(state, tid);

    if (now.unfireable) addUnfireable(state, tid);
    else removeUnfireable(state, tid);
}

void calculate_transition_list(BpnState* state, const BpnNet* net) {
    for (size_t tid = 0; tid < net->transition_count; ++tid) {
        FireableResult now = checkFireable(state, net, tid);

        if (now.fireable) {
            state->fire.count += 1;
            BpnIndexList_push(&state->fire.active, tid);
            state->fire.added_count += 1;
            BpnIndexList_push(&state->fire.added, tid);
        }

        if (now.unfireable) {
            state->unfire.count += 1;
            BpnIndexList_push(&state->unfire.active, tid);
            state->unfire.added_count += 1;
            BpnIndexList_push(&state->unfire.added, tid);
        }
    }
}

/* HEADER */

/** Net **/

/*** default ***/

void bpnCreateNet(BpnNet* net) {
    net->transition_count = 0;
    net->transitions_size = 0;
    net->transitions = NULL;
    net->reusable_transitions = NULL;
    net->place_count = 0;
    net->places_size = 0;
    net->places = NULL;
    net->initial_activated = NULL;
    net->reusable_places = NULL;
}

void bpnCloneNet(BpnNet* net_clone, const BpnNet* net) {
    net_clone->transition_count = net->transition_count;
    net_clone->transitions_size = net->transition_count;
    net_clone->transitions = malloc(net->transition_count * sizeof(BpnNode));
    CHECK_MEMORY_ERROR(net_clone->transitions);
    for (size_t tid = 0; tid < net->transition_count; ++tid) {
        bpnCloneNode(&net_clone->transitions[tid], &net->transitions[tid]);
    }
    net_clone->place_count = net->place_count;
    net_clone->places_size = net->place_count;
    net_clone->places = malloc(net->place_count * sizeof(BpnNode));
    CHECK_MEMORY_ERROR(net_clone->places);
    for (size_t pid = 0; pid < net->place_count; ++pid) {
        bpnCloneNode(&net_clone->places[pid], &net->places[pid]);
    }
    net_clone->initial_activated = malloc(net->place_count * sizeof(bool));
    CHECK_MEMORY_ERROR(net_clone->initial_activated);
    memcpy(net_clone->initial_activated, net->initial_activated, net->place_count * sizeof(bool));
    net_clone->reusable_transitions = NULL;
    bpnCloneIndexList(&net_clone->reusable_transitions, net->reusable_transitions);
    net_clone->reusable_places = NULL;
    bpnCloneIndexList(&net_clone->reusable_places, net->reusable_places);
}

bool bpnLoadNet(BpnNet* net, size_t count, const uint32_t* values) {
    size_t required_count = 2;

    if (count < required_count) return false;

    size_t index = 0;

    net->place_count = values[index++];
    required_count += net->place_count;
    if (count < required_count) return false;
    net->places_size = net->place_count;
    net->places = malloc(net->place_count * sizeof(BpnNode));
    CHECK_MEMORY_ERROR(net->places);
    net->initial_activated = malloc(net->place_count * sizeof(bool));
    CHECK_MEMORY_ERROR(net->initial_activated);
    for (size_t pid = 0; pid < net->place_count; ++pid)
        net->initial_activated[pid] = values[index++];

    net->transition_count = values[index++];
    required_count += 2 * net->transition_count;
    if (count < required_count) return false;
    net->transitions_size = net->transition_count;
    net->transitions = malloc(net->transition_count * sizeof(BpnNode));
    CHECK_MEMORY_ERROR(net->transitions);
    net->reusable_transitions = NULL;
    for (size_t tid = 0; tid < net->transition_count; ++tid) {
        BpnNode* transition = &net->transitions[tid];

        transition->next_count = values[index++];
        required_count += transition->next_count;
        if (count < required_count) return false;
        transition->next_size = transition->next_count;
        transition->next = malloc(transition->next_count * sizeof(size_t));
        CHECK_MEMORY_ERROR(transition->next);
        for (size_t i = 0; i < transition->next_count; ++i)
            transition->next[i] = values[index++];

        transition->prev_count = values[index++];
        required_count += transition->prev_count;
        if (count < required_count) return false;
        transition->prev_size = transition->prev_count;
        transition->prev = malloc(transition->prev_count * sizeof(size_t));
        CHECK_MEMORY_ERROR(transition->prev);
        for (size_t i = 0; i < transition->prev_count; ++i)
            transition->prev[i] = values[index++];

        if (!transition->next_count && !transition->prev_count) {
            BpnIndexList_push(&net->reusable_transitions, tid);
        }
    }

    size_t* place_next_counts = calloc(net->place_count, sizeof(size_t));
    CHECK_MEMORY_ERROR(place_next_counts);
    BpnIndexList** place_next = calloc(net->place_count, sizeof(BpnIndexList*));
    CHECK_MEMORY_ERROR(place_next);
    size_t* place_prev_counts = calloc(net->place_count, sizeof(size_t));
    CHECK_MEMORY_ERROR(place_prev_counts);
    BpnIndexList** place_prev = calloc(net->place_count, sizeof(BpnIndexList*));
    CHECK_MEMORY_ERROR(place_prev);

    net->reusable_places = NULL;
    for (size_t tid = 0; tid < net->transition_count; ++tid) {
        BpnNode* transition = &net->transitions[tid];
        for (size_t i = 0; i < transition->next_count; ++i) {
            size_t next = transition->next[i];
            ++place_prev_counts[next];
            BpnIndexList_push(&place_prev[next], tid);
        }
        for (size_t i = 0; i < transition->prev_count; ++i) {
            size_t prev = transition->prev[i];
            ++place_next_counts[prev];
            BpnIndexList_push(&place_next[prev], tid);
        }
    }

    for (size_t pid = 0; pid < net->place_count; ++pid) {
        BpnNode* place = &net->places[pid];
        place->next_count = place_next_counts[pid];
        place->next = malloc(place->next_count * sizeof(size_t));
        place->next_size = place->next_count;
        CHECK_MEMORY_ERROR(place->next);
        {
            int i = 0;
            BpnIndexList* list = place_next[pid];
            while(list != NULL) {
                place->next[i] = list->index;
                ++i;
                list = list->next;
            }
        }
        place->prev_count = place_prev_counts[pid];
        place->prev_size = place->prev_count;
        place->prev = malloc(place->prev_count * sizeof(size_t));
        CHECK_MEMORY_ERROR(place->prev);
        {
            int i = 0;
            BpnIndexList* list = place_prev[pid];
            while(list != NULL) {
                place->prev[i] = list->index;
                ++i;
                list = list->next;
            }
        }
        if (!place->next_count && !place->prev_count && !net->initial_activated[pid]) {
            BpnIndexList_push(&net->reusable_places, pid);
        }
    }

    for (size_t pid = 0; pid < net->place_count; ++pid) {
        bpnDestroyIndexList(place_next[pid]);
        bpnDestroyIndexList(place_prev[pid]);
    }
    free(place_next_counts);
    free(place_next);
    free(place_prev_counts);
    free(place_prev);

    return true;
}

void bpnDestroyNet(BpnNet* net) {
    for (size_t tid = 0; tid < net->transition_count; ++tid) {
        BpnNode* transition = &net->transitions[tid];
        bpnDeleteNode(transition);
    }
    for (size_t pid = 0; pid < net->place_count; ++pid) {
        BpnNode* place = &net->places[pid];
        bpnDeleteNode(place);
    }
    free(net->transitions);
    free(net->places);
    free(net->initial_activated);

    bpnDestroyIndexList(net->reusable_transitions);
    bpnDestroyIndexList(net->reusable_places);
}

size_t bpnNet_serializeSize(const BpnNet* net) {
    size_t count = 2 + net->place_count + 2 * net->transition_count;

    for (size_t tid = 0; tid < net->transition_count; ++tid) {
        BpnNode* transition = &net->transitions[tid];
        count += transition->next_count + transition->prev_count;
    }

    return count;
}

void bpnNet_serialize(const BpnNet* net, uint32_t* values) {
    size_t index = 0;

    values[index++] = net->place_count;
    for (size_t pid = 0; pid < net->place_count; ++pid)
        values[index++] = net->initial_activated[pid];

    values[index++] = net->transition_count;
    for (size_t tid = 0; tid < net->transition_count; ++tid) {
        BpnNode* transition = &net->transitions[tid];
        values[index++] = transition->next_count;
        for (size_t i = 0; i < transition->next_count; ++i)
            values[index++] = transition->next[i];
        values[index++] = transition->prev_count;
        for (size_t i = 0; i < transition->prev_count; ++i)
            values[index++] = transition->prev[i];
    }
}

/*** edit ***/

size_t get_pid(BpnNet* net) {
    size_t pid;
start:
    if (net->reusable_places == NULL) {
        pid = net->place_count++;
        if (net->place_count > net->places_size) {
            net->places_size = net->place_count + net->place_count / 5;
            net->places = realloc(net->places, net->places_size * sizeof(BpnNode));
            CHECK_MEMORY_ERROR(net->places);
            net->initial_activated = realloc(net->initial_activated, net->places_size * sizeof(bool));
            CHECK_MEMORY_ERROR(net->initial_activated);
        }
    }
    else {
        pid = BpnIndexList_pop(&net->reusable_places);
        BpnNode* place = &net->places[pid];
        if (place->next_count || place->prev_count || net->initial_activated[pid]) goto start;
    }
    return pid;
}

size_t get_tid(BpnNet* net) {
    size_t tid;
start:
    if (net->reusable_transitions == NULL)
        ADD_ELEMENT(tid, net->transition_count, net->transitions_size, net->transitions)
    else {
        tid = BpnIndexList_pop(&net->reusable_transitions);
        BpnNode* transition = &net->transitions[tid];
        if (transition->next_count || transition->prev_count) goto start;
    }
    return tid;
}

size_t bpnNet_addPlace(BpnNet* net) {
    size_t pid = get_pid(net);

    BpnNode* node = &net->places[pid];
    bpnCreateNode(node);

    net->initial_activated[pid] = 0;

    return pid;
}

size_t bpnNet_addTransition(BpnNet* net) {
    size_t tid = get_tid(net);

    BpnNode* node = &net->transitions[tid];
    bpnCreateNode(node);

    return tid;
}

size_t bpnNet_addConnectedTransition(BpnNet* net, size_t place_count, size_t* pids) {
    size_t tid = bpnNet_addTransition(net);
    for (size_t i = 0; i < place_count; ++i) {
        size_t pid = pids[i];
        bpnNet_connectIn_unsafe(net, tid, pid);
    }

    return tid;
}

void removeNode(size_t id, BpnNode* nodes, BpnNode* link_nodes) {
    BpnNode* node = &nodes[id];

    for (size_t i = 0; i < node->prev_count; ++i) {
        size_t link_id = node->prev[i];
        BpnNode* link_node = &link_nodes[link_id];
        removeIndex(id, &link_node->next_count, link_node->next);
    }

    for (size_t i = 0; i < node->next_count; ++i) {
        size_t link_id = node->next[i];
        BpnNode* link_node = &link_nodes[link_id];
        removeIndex(id, &link_node->prev_count, link_node->prev);
    }

    bpnDeleteNode(node);
}

void bpnNet_removePlace(BpnNet* net, size_t pid) {
    removeNode(pid, net->places, net->transitions);
    net->initial_activated[pid] = 0;
    BpnIndexList_appendNew(&net->reusable_places, pid);
}

void bpnNet_removeTransition_unsafe(BpnNet* net, size_t tid) {
    removeNode(tid, net->transitions, net->places);
    BpnIndexList_appendNew(&net->reusable_transitions, tid);
}

bool bpnNet_connectIn_unsafe(BpnNet* net, size_t tid, size_t pid) {
    BpnNode* transition = &net->transitions[tid];
    BpnNode* place = &net->places[pid];

    for (size_t i = 0; i < transition->prev_count; ++i) {
        size_t current_pid = transition->prev[i];

        if (current_pid < pid) continue;
        else if (current_pid == pid) return false;

        transition->prev[i] = pid;
        pid = current_pid;
    }

    for (size_t i = 0; i < place->next_count; ++i) {
        size_t current_tid = place->next[i];

        if (current_tid < tid) continue;

        place->next[i] = tid;
        tid = current_tid;
    }

    size_t prev_index, next_index;
    ADD_ELEMENT(prev_index, transition->prev_count, transition->prev_size, transition->prev)
    ADD_ELEMENT(next_index, place->next_count, place->next_size, place->next)

    transition->prev[prev_index] = pid;
    place->next[next_index] = tid;

    return true;
}

bool bpnNet_connectOut(BpnNet* net, size_t tid, size_t pid) {
    BpnNode* transition = &net->transitions[tid];
    BpnNode* place = &net->places[pid];

    for (size_t i = 0; i < transition->next_count; ++i) {
        size_t current_pid = transition->next[i];

        if (current_pid < pid) continue;
        else if (current_pid == pid) return false;

        transition->next[i] = pid;
        pid = current_pid;
    }

    for (size_t i = 0; i < place->prev_count; ++i) {
        size_t current_tid = place->prev[i];

        if (current_tid < tid) continue;

        place->prev[i] = tid;
        tid = current_tid;
    }

    size_t prev_index, next_index;
    ADD_ELEMENT(next_index, transition->next_count, transition->next_size, transition->next)
    ADD_ELEMENT(prev_index, place->prev_count, place->prev_size, place->prev)

    transition->next[next_index] = pid;
    place->prev[prev_index] = tid;

    return true;
}

void disconnectNodes(size_t first_id, BpnNode* first_nodes, size_t second_id, BpnNode* second_nodes) {
    BpnNode* first_node = &first_nodes[first_id];
    BpnNode* second_node = &second_nodes[second_id];

    removeIndex(second_id, &first_node->next_count, first_node->next);
    removeIndex(first_id, &second_node->prev_count, second_node->prev);
}

void bpnNet_disconnectIn(BpnNet* net, size_t tid, size_t pid) {
    disconnectNodes(pid, net->places, tid, net->transitions);
}

void bpnNet_disconnectOut_unsafe(BpnNet* net, size_t tid, size_t pid) {
    disconnectNodes(tid, net->transitions, pid, net->places);
}


size_t bpnNet_duplicateTransition(BpnNet* net, size_t tid) {
    size_t clone_tid = get_tid(net);

    BpnNode* node = &net->transitions[tid];
    BpnNode* node_clone = &net->transitions[clone_tid];
    bpnCloneNode(node_clone, node);

    for (size_t i = 0; i < node->prev_count; ++i) {
        size_t pid = node->prev[i];
        BpnNode* node = &net->places[pid];
        size_t new_tid = sortIndex(clone_tid, node->next_count, node->next);
        size_t id;
        ADD_ELEMENT(id, node->next_count, node->next_size, node->next)
        node->next[id] = new_tid;
    }

    for (size_t i = 0; i < node->next_count; ++i) {
        size_t pid = node->next[i];
        BpnNode* node = &net->places[pid];
        size_t new_tid = sortIndex(clone_tid, node->prev_count, node->prev);
        size_t id;
        ADD_ELEMENT(id, node->prev_count, node->prev_size, node->prev)
        node->prev[id] = new_tid;
    }

    return clone_tid;
}

size_t bpnNet_duplicatePlace(BpnNet* net, size_t pid) {
    size_t clone_pid = get_pid(net);

    BpnNode* node = &net->places[pid];
    BpnNode* node_clone = &net->places[clone_pid];

    bpnCloneNode(node_clone, node);

    net->initial_activated[clone_pid] = net->initial_activated[pid];

    for (size_t i = 0; i < node->prev_count; ++i) {
        size_t tid = node->prev[i];
        BpnNode* node = &net->transitions[tid];
        size_t new_pid = sortIndex(clone_pid, node->next_count, node->next);
        size_t id;
        ADD_ELEMENT(id, node->next_count, node->next_size, node->next)
        node->next[id] = new_pid;
    }

    for (size_t i = 0; i < node->next_count; ++i) {
        size_t tid = node->next[i];
        BpnNode* node = &net->transitions[tid];
        size_t new_pid = sortIndex(clone_pid, node->prev_count, node->prev);
        size_t id;
        ADD_ELEMENT(id, node->prev_count, node->prev_size, node->prev)
        node->prev[id] = new_pid;
    }

    return clone_pid;
}

void bpnNet_activate(BpnNet* net, size_t pid) {
    bool* count = &net->initial_activated[pid];
    if (*count) return; 
    *count = true;
}

/** State **/

/*** helpers ***/

void bpnFinalizeState(BpnState* state, const BpnNet* net) {
    state->transitions_size = net->transition_count;
    state->places_size = net->place_count;

    bpnCreateFireChanges(&state->fire);
    bpnCreateFireChanges(&state->unfire);

    calculate_transition_list(state, net);
}

/*** default ***/

void bpnCreateState(BpnState* state, const BpnNet* net) {
    state->activated = calloc(net->place_count, sizeof(bool));
    CHECK_MEMORY_ERROR(state->activated);

    state->call_states = calloc(net->transition_count, sizeof(bool));
    CHECK_MEMORY_ERROR(state->call_states);

    bpnFinalizeState(state, net);
}

void bpnCloneState(BpnState* state_clone, const BpnState* state, const BpnNet* net) {
    state_clone->activated = malloc(net->place_count * sizeof(bool));
    CHECK_MEMORY_ERROR(state_clone->activated);
    memcpy(state_clone->activated, state->activated, net->place_count * sizeof(bool));

    state_clone->call_states = malloc(net->transition_count * sizeof(bool));
    CHECK_MEMORY_ERROR(state_clone->call_states);
    memcpy(state_clone->call_states, state->call_states, net->transition_count * sizeof(bool));

    bpnFinalizeState(state_clone, net);
}

bool bpnLoadState(BpnState* state, const BpnNet* net, size_t count, const bool* values) {
    if (count != net->transition_count) return false;

    state->activated = calloc(net->place_count, sizeof(bool));
    CHECK_MEMORY_ERROR(state->activated);

    state->call_states = malloc(net->transition_count * sizeof(bool));
    CHECK_MEMORY_ERROR(state->call_states);

    for (size_t tid = 0; tid < net->transition_count; ++tid) {
        state->call_states[tid] = values[tid];

        BpnNode* transition = &net->transitions[tid];
        for (size_t i = 0; i < transition->prev_count; ++i) {
            size_t pid = transition->prev[i];
            state->activated[pid] ^= state->call_states[tid];
        }
        for (size_t i = 0; i < transition->next_count; ++i) {
            size_t pid = transition->next[i];
            state->activated[pid] ^= state->call_states[tid];
        }
    }

    bpnFinalizeState(state, net);

    return true;
}

void bpnDestroyState(BpnState* state) {
    free(state->activated);
    free(state->call_states);
    bpnDestroyFireChanges(&state->fire);
    bpnDestroyFireChanges(&state->unfire);
}

/*** simulate ***/

void getTransitions(size_t current_count, BpnIndexList* list, size_t* count, size_t* transitions) {
    if (!transitions) {
        *count = current_count;
        return;
    }
    for (size_t i = 0; i < *count && list != NULL; ++i) {
        transitions[i] = list->index;
        list = list->next;
    }
    if (list == NULL) *count = current_count;
}

void bpnState_transitions(BpnState* state, size_t* count, size_t* transitions) {
    getTransitions(state->fire.count, state->fire.active, count, transitions);
}

void bpnState_transitions_backwards(BpnState* state, size_t* count, size_t* transitions) {
    getTransitions(state->unfire.count, state->unfire.active, count, transitions);
}

void bpnState_cleanChanges(BpnState* state) {
    state->fire.added_count = 0;
    bpnClearIndexList(&state->fire.added);
    state->fire.removed_count = 0;
    bpnClearIndexList(&state->fire.removed);
}

void bpnState_cleanChanges_backwards(BpnState* state) {
    state->unfire.added_count = 0;
    bpnClearIndexList(&state->unfire.added);
    state->unfire.removed_count = 0;
    bpnClearIndexList(&state->unfire.removed);
}

void popTransitions(size_t* current_count, BpnIndexList** list, size_t* count, size_t* transitions) {
    if (!transitions) {
        *count = *current_count;
        return;
    }
    for (size_t i = 0; i < *count && list != NULL; ++i) transitions[i] = BpnIndexList_pop(list);
    if (list == NULL) *count = *current_count;
    *current_count -= *count;
}

void bpnState_addedTransitions(BpnState* state, size_t* count, size_t* transitions) {
    popTransitions(&state->fire.added_count, &state->fire.added, count, transitions);
}

void bpnState_addedTransitions_backwards(BpnState* state, size_t* count, size_t* transitions) {
    popTransitions(&state->unfire.added_count, &state->unfire.added, count, transitions);
}

void bpnState_removedTransitions(BpnState* state, size_t* count, size_t* transitions) {
    popTransitions(&state->fire.removed_count, &state->fire.removed, count, transitions);
}

void bpnState_removedTransitions_backwards(BpnState* state, size_t* count, size_t* transitions) {
    popTransitions(&state->unfire.removed_count, &state->unfire.removed, count, transitions);
}

void bpnState_fire(BpnState* state, const BpnNet* net, size_t tid) {
    state->call_states[tid] ^= true;

    BpnNode* transition = &net->transitions[tid];

    for (size_t i = 0; i < transition->prev_count; ++i) {
        state->activated[transition->prev[i]] ^= true;
    }

    for (size_t i = 0; i < transition->next_count; ++i) {
        state->activated[transition->next[i]] ^= true;
    }

    BpnIndexList* recalculate_indices = NULL;

    for (size_t i = 0; i < transition->prev_count; ++i) {
        size_t pid = transition->prev[i];
        BpnNode* place = &net->places[pid];
        for (size_t j = 0; j < place->next_count; ++j) {
            BpnIndexList_appendNew(&recalculate_indices, place->next[j]);
        }
        for (size_t j = 0; j < place->prev_count; ++j) {
            BpnIndexList_appendNew(&recalculate_indices, place->prev[j]);
        }
    }

    for (size_t i = 0; i < transition->next_count; ++i) {
        size_t pid = transition->next[i];
        BpnNode* place = &net->places[pid];
        for (size_t j = 0; j < place->next_count; ++j) {
            BpnIndexList_appendNew(&recalculate_indices, place->next[j]);
        }
        for (size_t j = 0; j < place->prev_count; ++j) {
            BpnIndexList_appendNew(&recalculate_indices, place->prev[j]);
        }
    }

    while (recalculate_indices) {
        recalculate_transition(state, net, recalculate_indices->index);
        BpnIndexList* current = recalculate_indices;
        recalculate_indices = recalculate_indices->next;
        free(current);
    }
}

void bpnState_refresh(BpnState* state, const BpnNet* net) {
    if (state->places_size < net->place_count)
        RESIZE_ARRAY(net->place_count, state->places_size, state->activated)

    size_t new_transitions_start = state->transitions_size;
    if (state->transitions_size < net->transition_count) {
        RESIZE_ARRAY(net->transition_count, state->transitions_size, state->call_states)
        for (size_t tid = new_transitions_start; tid < state->transitions_size; ++tid) state->call_states[tid] = 0;
    }

    for (size_t pid = 0; pid < net->place_count; ++pid) {
        bool activated = false;
        BpnNode* place = &net->places[pid];
        for (size_t i = 0; i < place->prev_count; ++i) {
            size_t tid = place->prev[i];
            activated ^= state->call_states[tid];
        }
        for (size_t i = 0; i < place->next_count; ++i) {
            size_t tid = place->next[i];
            activated ^= state->call_states[tid];
        }
        state->activated[pid] = activated;
    }

    for (size_t tid = 0; tid < net->transition_count; ++tid) {
        recalculate_transition(state, net, tid);
    }
}

