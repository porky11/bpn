#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>

#include "../bpn.h"

void localTests() {
    BpnNet net;
    bpnCreateNet(&net);
    size_t pid_tmp = bpnNet_addPlace(&net);
    bpnNet_removePlace(&net, pid_tmp);
    printf("Current place count: %zu\n", net.place_count);
    size_t pid1 = bpnNet_addPlace(&net);
    size_t pid2 = bpnNet_addPlace(&net);
    size_t tid = bpnNet_addConnectedTransition(&net, 1, &pid1);
    bpnNet_connectOut(&net, tid, pid2);
    bpnNet_removePlace(&net, pid1);
    bpnNet_removePlace(&net, pid2);
    bpnDestroyNet(&net);
}

int main(int argc, char** argv) {
    localTests();

    BpnNet net;

    struct stat st; 

    if (stat("examples/example.pns", &st) != 0) {
        fprintf(stderr, "Cannot determine size of net file\n");
        exit(EXIT_FAILURE);
    }

    size_t size = (st.st_size + 3) / 4;
    uint32_t values[size];

    FILE* loadFile = fopen("examples/example.pns", "rb");
    fread(values, sizeof(uint32_t), size, loadFile);
    
    fclose(loadFile);

    if (!bpnLoadNet(&net, size, values)) {
        fprintf(stderr, "Failed loading of net\n");
        exit(EXIT_FAILURE);
    }

    if (argc >= 2) {
        if (!strncmp(argv[1], "net", 3)) {
            FILE* saveFile = fopen("examples/example.pns", "wb");

            if (!saveFile) {
                printf("Saving failed\n");
                exit(EXIT_FAILURE);
            } else {
                size_t count = bpnNet_serializeSize(&net);
                uint32_t elements[count];
                bpnNet_serialize(&net, elements);

                fwrite(elements, sizeof(size_t), count, saveFile);

                fclose(saveFile);

                printf("Saving successful\n");
                exit(EXIT_SUCCESS);
            }
        }
    }
    BpnNet net_clone;
    bpnCloneNet(&net_clone, &net);
    bpnDestroyNet(&net);
    net = net_clone;
    BpnState state;
    if (argc >= 2) {
        if (!strncmp(argv[1], "load", 4)) {
            struct stat st; 

            if (stat("examples/example.pns", &st) != 0) {
                fprintf(stderr, "Cannot determine size of state file\n");
                exit(EXIT_FAILURE);
            }

            size_t size = st.st_size;
            if (net.transitions_size < size) size = net.transitions_size;
            bool values[size];

            FILE *loadFile = fopen("examples/example.bpnl", "rb");
            if (!loadFile) {
                fprintf(stderr, "Opening state file for loading failed\n");
                exit(EXIT_FAILURE);
            }

            fread(values, sizeof(bool), size, loadFile);

            bool result = bpnLoadState(&state, &net, size, values);
            if (!result) {
                fprintf(stderr, "State initialization failed\n");
                exit(EXIT_FAILURE);
            }
        }
    } else {
        bpnCreateState(&state, &net);
    }
    BpnState state_clone;
    bpnCloneState(&state_clone, &state, &net);
    bpnDestroyState(&state);
    state = state_clone;

    size_t MAX_COUNT = 64;
    char names[MAX_COUNT * net.transition_count];

    FILE *file =
        fopen("examples/example.pnk","r");
    for (size_t i = 0; i < net.transition_count; ++i) {
        char* name = &names[i * MAX_COUNT];
        getline(&name, &MAX_COUNT, file);
    }
    fclose(file);

    bool forward = true;

loop: {
        size_t count;
        void (*state_transitions) (BpnState* state, size_t* count, size_t* transitions);
        if (forward) {
            state_transitions = bpnState_transitions;
        } else {
            state_transitions = bpnState_transitions_backwards;
        }
        state_transitions(&state, &count, NULL);
        if (count == 0) goto reverse;
        size_t transitions[count];
        state_transitions(&state, &count, transitions);
        printf("Choose a transition:\n");
        for(size_t i = 0; i < count; ++i) {
            printf("%lu: %s", i + 1, &names[transitions[i] * MAX_COUNT]);
        }
        char num[8];
        char* n = num;
        size_t bytes = 7;
        getline(&n, &bytes, stdin);
        if (*n == 10) goto loop;
        switch (*n) {
        case 'r':
            goto reverse;
        case 'e':
            goto end;
        case 's':
            {
                FILE *saveFile = fopen("examples/example.bpnl", "wb");
                if (!saveFile) {
                    fprintf(stderr, "Opening state file for saving\n");
                    exit(EXIT_FAILURE);
                }

                if (net.transition_count != fwrite(state.call_states, sizeof(bool), net.transition_count, saveFile)) {
                    fprintf(stderr, "Saving state failed\n");
                    exit(EXIT_FAILURE);
                }

                fclose(file);
            }

            printf("Saving state successful\n");

            goto loop;
        }
        size_t select = atoi(num);
        if (select != 0 && select <= count) {
            bpnState_fire(&state, &net, transitions[select - 1]);
        } else {
            printf("You have to input a valid number from 1 to %zu\n", count);
            printf("You can also stop by writing \"e\", reverse by writing \"r\" or save by writing \"s\"\n");
        }
    }
    goto loop;

reverse:
    forward = !forward;
    printf("Reverse play direction!\n");
    goto loop;

end:
    bpnDestroyState(&state);
    bpnDestroyNet(&net);
    return EXIT_SUCCESS;
}

