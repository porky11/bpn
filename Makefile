CC=clang --std=gnu17 -ferror-limit=3 -O3

.PHONY: run clean all install

libbpn.so: bpn.h bpn.c
	$(CC) -fPIC -shared bpn.c -o libbpn.so -Wall -Werror

all: bpn.o libbpn.so test

install: libbpn.so
	cp bpn.h /usr/local/include/
	cp libbpn.so /usr/local/lib/

run: test
	./test

test: testing/test.c bpn.o
	$(CC) testing/test.c -o test bpn.o -Wall -Werror

bpn.o: bpn.h bpn.c
	$(CC) -c bpn.c -o bpn.o -Wall -Werror

clean:
	rm -f test bpn.o libbpn.so examples/example.bpnl

