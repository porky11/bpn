#include <stdbool.h>
#include <stdint.h>

#ifndef BPN_H_
#define BPN_H_

/* TYPES */

typedef struct BpnNode {
    size_t next_count;
    size_t next_size;
    size_t* next;
    size_t prev_count;
    size_t prev_size;
    size_t* prev;
} BpnNode;

typedef struct BpnIndexList {
    size_t index;
    struct BpnIndexList* next;
} BpnIndexList;

typedef struct BpnNet {
    size_t transition_count;
    size_t transitions_size;
    BpnNode* transitions;
    BpnIndexList* reusable_transitions;
    size_t place_count;
    size_t places_size;
    BpnNode* places;
    bool* initial_activated;
    BpnIndexList* reusable_places;
} BpnNet;

typedef struct BpnFireChanges {
    size_t count, added_count, removed_count;
    BpnIndexList* active, * added, * removed;
} BpnFireChanges;

typedef struct BpnState {
    size_t places_size;
    bool* activated;
    size_t transitions_size;
    bool* call_states;
    BpnFireChanges fire;
    BpnFireChanges unfire;
} BpnState;

/* FUNCTIONS */

/** Net **/

/*** default ***/

/// initialize a new `Net`
void bpnCreateNet(BpnNet* net);
/// make a (deep) copy of a `Net`
void bpnCloneNet(BpnNet* net_clone, const BpnNet* net);
/// load a new `Net` from a u32 array; return value indicates success
bool bpnLoadNet(BpnNet* net, size_t count, const uint32_t* values);
/// destructor for `Net`
void bpnDestroyNet(BpnNet* net);
/// calculate the number of required u32 values for saving
size_t bpnNet_serializeSize(const BpnNet* net);
/// save `Net` to u32 array
void bpnNet_serialize(const BpnNet* net, uint32_t* values);

/*** edit ***/

/// add new place to a `Net` and return id
size_t bpnNet_addPlace(BpnNet* net);
/// add new transition to a `Net` and return its id
/// Caution: rather use `addConnectedTransition` when you want to extend the net safely (see safety.md)
size_t bpnNet_addTransition(BpnNet* net);
/// adds a new transition to a `Net` and connects it to specified places; useful for safety reasons (see safety.md)
size_t bpnNet_addConnectedTransition(BpnNet* net, size_t place_count, size_t* pids);
/// removes place `pid` from `net`
void bpnNet_removePlace(BpnNet* net, size_t pid);
/// removes transition `tid` from `net`
void bpnNet_removeTransition_unsafe(BpnNet* net, size_t tid);
/// create a new input connection of transition `tid` from place `pid`
bool bpnNet_connectIn_unsafe(BpnNet* net, size_t tid, size_t pid);
/// create a new output connection of transition `tid` to place `pid`
bool bpnNet_connectOut(BpnNet* net, size_t tid, size_t pid);
/// remove the input connection of transition `tid` from place `pid`
void bpnNet_disconnectIn(BpnNet* net, size_t tid, size_t pid);
/// remove the output connection of transition `tid` to place `pid`
void bpnNet_disconnectOut_unsafe(BpnNet* net, size_t tid, size_t pid);
/// duplicates a place, sharing all input and output places
size_t bpnNet_duplicateTransition(BpnNet* net, size_t tid);
/// duplicates a place, sharing all input and output transitions
size_t bpnNet_duplicatePlace(BpnNet* net, size_t pid);
/// activates the place specified by `pid`
void bpnNet_activate(BpnNet* net, size_t pid);

/** State **/

/*** default ***/

/// initialize a new `State`
void bpnCreateState(BpnState* state, const BpnNet* net);
/// make a (deep) copy of a `State` (sharing the same `Net`)
void bpnCloneState(BpnState* state_clone, const BpnState* state, const BpnNet* net);
/// load a new `State` from a bool array; return value indicates success
bool bpnLoadState(BpnState* state, const BpnNet* net, size_t count, const bool* values);
/// destructor for `State`
void bpnDestroyState(BpnState* state);

/*** simulate ***/

/// get a list of currently available transitions
/// when `transitions` is `NULL`, `count will be set to the transition count
/// else `transitions` will be filled with up to `count` elements
/// `count` will be set to the indices in `transitions` and might be lower than specified
void bpnState_transitions(BpnState* state, size_t* count, size_t* transitions);
void bpnState_transitions_backwards(BpnState* state, size_t* count, size_t* transitions);
/// get a list of newly added transitions in specified direction
/// all written transitions will be synchronized and won't be processed until another change
void bpnState_addedTransitions(BpnState* state, size_t* count, size_t* transitions);
void bpnState_addedTransitions_backwards(BpnState* state, size_t* count, size_t* transitions);
/// get a list of newly removed transitions in specified direction
/// all written transitions will be synchronized and won't be processed until another change
void bpnState_removedTransitions(BpnState* state, size_t* count, size_t* transitions);
void bpnState_removedTransitions_backwards(BpnState* state, size_t* count, size_t* transitions);
/// this will remove all transition changes
/// Caution: only use after querying the current transitions directly and manually synchronizing your local state
void bpnState_cleanChanges(BpnState* state);
void bpnState_cleanChanges_backwards(BpnState* state);
/// fire the transition
/// Caution: ensure to only call valid transitions
/// (queried using the methods transitions or managed yourself using addedTransitions and removedTransitions)
void bpnState_fire(BpnState* state, const BpnNet* net, size_t tid);

/*** update ***/

/// refreshes prtri net after edits
void bpnState_refresh(BpnState* state, const BpnNet* net);

#endif
