# Petri net simulator for C

This is a program to edit and simulate binary petri nets in C.
The reason for C is portability, especially language portability.

It's probably the only library for petri net simulation in existence.
Simulation is fast and scalable, since it's not neccessary to iterate over all nodes everytime a transition has fired.

## Documentation

### Naming conventions

All types and functions use camel case. Types are prefixed with uppercase "Bpn", functions with lowercase "bpn".
Functions meant to be used as constructors and similar end with the name of the created type (omitting the "Bpn" prefix).
Functions representing methods of a type start with the typename (but lowercase) and end with the method name, both separated by a "_".
An additional "_unsafe" and _"backwards" suffixes added to some methods are discussed later.

### Data structures

The petri net is stored in the type `BpnNet`. It contains information about the existing places and transitions and also stores the initial token count for each place.

In order to simulate the petri net, there is another data structure called `BpnState`, which stores the current distribution of the tokens inside the places and some other infos.

### Initialization

#### Initializing a petri net

A petri net can be loaded from a file. This is done by calling `bool bpnLoadNet (BpnNet* net, const char* filename)`.
Therefore you have to supply the petri net to be filled and the filename, where the net will be loaded.
The return value will be true, when the initialization was successful.

The file only contains 32-bit fields and has the following format:

* the count of places
* the initial token count for every place
* the count of transitions
* for each transition:
  - number of following places
  - the id of every following place
  - number of preceding places
  - the id of every preceding place

The recommended extension for this file format is ".pns", which stands for "`p`etri `n`et `s`tructure".

It's also possible to initialize the petri net without loading using `void bpnInitNet (BpnNet* net)`.
It's not useful in the current version, because there's no easy way to extend it at runtime. When it's implemented in future versions, this will be very practical.

#### Initiaizing the state

In order to initialize a state, there already needs to be a petri net. It is done by calling `void bpnInitState (BpnState* state)`.
The first argument will be the state, you want to initialize, the second argument is the previously created petri net.

### Destructors

Both, `BpnNet` and `BpnState` need to be destroyed, when not used anymore. This can be done using `void bpnDestroyNet(BpnNet* net)` and `void bpnDestroyState(BpnState* state)`. It's recommended to destroy all states before destroying the petri net. Using a petri net state, when the petri net is already destroyed will cause errors.

### Editing

It's also possible to edit the petri net manually using various methods.
Some of these methods may invalidate existing paths. These are suffixed with "_unsafe". See [safety.md](safety.md) for more information.

### Running the simulation

After creating a state, the simulation can begin.

#### Querying fireable transitions

First you have to get some transition, that currently is fireable.
A list of them can be queried by calling `void bpnState_transitions (BpnState* state, size_t* count, size_t* transitions)`.
It takes the previous created state and a pointer to a `count`. If `transitions` is a null pointer, the count of available transitions will be written into `count`.
If `transitions` is specified, up to `count` transitions will be written into `transitions` and then `count` will be set to the number of transitions  written.
Most likely you will call this function twice, once to get the count, then allocate the required memory and then call it again.
Instead you can also just call it once, and specify the memory you already have allocated. This way you may not get all transitions.

Also see following methods (described in the [header](bpn.h)):
* `void bpnState_addedTransitions (BpnState* state, size_t* count, size_t* transitions)`
* `void bpnState_removedTransitions (BpnState* state, size_t* count, size_t* transitions)`
* `void bpnState_cleanChanges (BpnState* state)`

#### Fire transitions

After you got some transitions, you can select one and fire it by calling `void bpnTransition_fire (const BpnTransition tid, BpnState* const state)`.

This will change, which transitions can fire, so after fireing a transition you should query transitions again, before fireing another time.
For performance reasons, this is not checked at runtime.

#### Strategies for selecting transitions

You can imagine many ways to select the transition. The easiest way is just getting the first transition. This is a bad choice, since it's neither random nor controllable by you and may even change in future versions or when using different structures of the same game.

So the simplest useful strategy may be random selection. This may help to detect race conditions in programs modelled by petri nets.

Another way is just letting the user decide the transition.

Other useful strategies need you to connect some external information with the transitions.
For example, every transition can be connected with some textual identifier or even a story, which can be selected.
It could also be connected with external events, which trigger some transitions.

When information is connected with transitions, selecting a transition can also trigger external events.
Even when a new transition can be fired, this may already trigger events.

#### Playing backwards

It's possible to simulate everything backwards.
Therefore every simulation function has a counterpart with a "_backwards" suffix.
These functions are used in the same way as their default counterparts.
This behaviour can be abused to produce some fancy relations.

#### Interactive editing

When editing the net after a state is created, there are a few restrictions, if you don't want to invalidate the state:
* it's not allowed to call functions marked with "_unsafe"
* it's recommended to clear edits using `void bpnNet_clearEdits(BpnNet* net)` after the state is updated for performance reasons
* the state is updated automatically when calling simulation methods
* clearing edits before updating states may invalidate them
* it can be ensured, that safe edits won't make any fireable transition not fireable anymore; this applies to both simulation directions

### Generating files

The most straightforward way to generate a ".bpn" file is generating the petri net using functions of this library to build and save.

There's already [a GUI](https://gitlab.com/porky11/pn-editor) using this library to simplify building it without the need of programming.

Another way is using [this lisp program](https://gitlab.com/porky11/simple-petri-net). It's still more powerful than this program, so if you don't fear common lisp, you should try this instead.

## Planned Extensions

Currently no extensions are planned.

But an alternative, which works basically the same, but has less methods for editing, is planned.

## See also

* [Safe rust wrapper](https://gitlab.com/porky11/pnrs)

